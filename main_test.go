package main

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestLoadEnv(t *testing.T) {
	loadEnv()
	assert.Equal(t, DefaultMaxPictures, config.maxPictures)
	assert.Equal(t, DefaultPictureURL, config.pictureURL)

	os.Setenv("MAX_NUMBER_OF_PICTURES", "sdvsd")
	assert.Panics(t, loadEnv)

	os.Setenv("MAX_NUMBER_OF_PICTURES", "42")
	os.Setenv("MAX_PICTURE_SOURCE_URL", "abcdef")
	loadEnv()
	assert.Equal(t, 42, config.maxPictures)
	assert.Equal(t, "abcdef", config.pictureURL)
}

func TestHandler(t *testing.T) {
	r, _ := http.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handler)

	handler.ServeHTTP(rr, r)
	assert.Equal(t, http.StatusOK, rr.Code)
}

func TestHandleStatus(t *testing.T) {
	r, _ := http.NewRequest("GET", "/status", nil)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handleStatus)

	handler.ServeHTTP(rr, r)
	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, "app is running", rr.Body.String())
}
